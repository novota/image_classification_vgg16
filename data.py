
import os
import pickle
import torch.utils.data
import torchvision.transforms.transforms as transforms

class ClassificationDataset(torch.utils.data.Dataset):
	def __init__(self, pkl_name, augmentation=True):
		self.augmentation = augmentation
		self.pkl_name = pkl_name
		data_path = os.path.abspath(f"/local/temporary/vir/hw2/{self.pkl_name}")
		with open(data_path, "rb") as f:
			loaded_data = pickle.load(f)


		# define data transformation
		self.transform = transforms.Compose([transforms.ToPILImage(), transforms.RandomVerticalFlip(), transforms.RandomHorizontalFlip(), transforms.ColorJitter(hue=.3, saturation=.3),
							transforms.ToTensor()])
		self.transform2 = transforms.Compose([transforms.ToPILImage(), transforms.ToTensor()])

		self.labels = loaded_data['labels'].astype("long")
		self.data = loaded_data['data']
		self.file_names = loaded_data['filenames']

	def __getitem__(self, index):
		"""
		:param index: nbr of the data sample to be taken from the data loader
		:return: input data to the neural network
		"""
		one_data_sample = self.data[index]
		if self.augmentation:
			one_data_sample = self.transform(one_data_sample)
		else:
			one_data_sample = self.transform2(one_data_sample)
		one_label_sample = self.labels[index]

		batch = {'data' : one_data_sample,
			'label' : one_label_sample,
			'index' : index}

		return batch


	def __len__(self):
		"""
		:return: maximal index
		"""

		return self.labels.shape[0]
