import data
import os
import torch
import torch.nn as nn
import torch.optim
import torch.utils.data
import torchvision.transforms.transforms as transforms

def init_weights(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)


def norm_input(x):
    std = x.std(dim=(2,3))
    mean = x.mean(dim=(2,3))
    std = std.unsqueeze(-1).unsqueeze(-1)
    mean = mean.unsqueeze(-1).unsqueeze(-1)
    x = (x - mean) / (std + 0.0000001)
    return x

class Model(nn.Module):
    """Modified VGG16 net, only 2 linear layers and both without Relu. No bias for conv layers, SGD optimizer and CrossEntropyLoss, data augmentation such as, horizontal and vertical flip and others."""
    def __init__(self, in_channels: int, bias=False):
        super(Model, self).__init__()

        self.relu = nn.ReLU()
        self.bn0 = nn.BatchNorm2d(num_features=3, affine=False)

        self.conv_1 = nn.Conv2d(in_channels=in_channels, out_channels=64, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_1 = nn.BatchNorm2d(num_features=64, affine=True)
        self.conv_2 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_2 = nn.BatchNorm2d(num_features=64, affine=True)

        self.max_pool_1 = nn.MaxPool2d(kernel_size=(2, 2), stride=2)

        self.conv_3 = nn.Conv2d(in_channels=64, out_channels=128, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_3 = nn.BatchNorm2d(num_features=128, affine=True)
        self.conv_4 = nn.Conv2d(in_channels=128, out_channels=128, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_4 = nn.BatchNorm2d(num_features=128, affine=True)

        self.max_pool_2 = nn.MaxPool2d(kernel_size=(2, 2), stride=2)

        self.conv_5 = nn.Conv2d(in_channels=128, out_channels=256, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_5 = nn.BatchNorm2d(num_features=256, affine=True)
        self.conv_6 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_6 = nn.BatchNorm2d(num_features=256, affine=True)
        self.conv_7 = nn.Conv2d(in_channels=256, out_channels=256, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_7 = nn.BatchNorm2d(num_features=256, affine=True)

        self.max_pool_3 = nn.MaxPool2d(kernel_size=(2, 2), stride=2)

        self.conv_8 = nn.Conv2d(in_channels=256, out_channels=512, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_8 = nn.BatchNorm2d(num_features=512, affine=True)
        self.conv_9 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_9 = nn.BatchNorm2d(num_features=512, affine=True)
        self.conv_10 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_10 = nn.BatchNorm2d(num_features=512, affine=True)

        self.max_pool_4 = nn.MaxPool2d(kernel_size=(2, 2), stride=2)

        self.conv_11 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_11 = nn.BatchNorm2d(num_features=512, affine=True)
        self.conv_12 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_12 = nn.BatchNorm2d(num_features=512, affine=True)
        self.conv_13 = nn.Conv2d(in_channels=512, out_channels=512, kernel_size=(3, 3), stride=1, padding=1, bias=bias)
        self.batch_norm_13 = nn.BatchNorm2d(num_features=512, affine=True)

        self.max_pool_5 = nn.MaxPool2d(kernel_size=(2, 2), stride=2)

        self.linear_14 = nn.Linear(in_features=8192, out_features=4096)
        self.drop_out_14 = nn.Dropout()
        self.linear_15 = nn.Linear(in_features=4096, out_features=10)
        self.drop_out_15 = nn.Dropout()
        self.linear_16 = nn.Linear(in_features=4096, out_features=10)

        self.softmax = nn.Softmax(dim=1)

        self._initialize_weights()

    def forward(self, x: torch.Tensor):
        x = x.view(x.shape[0], 3, 128, 128)

        x = self.bn0(x)

        x = self.conv_1(x)
        x = self.batch_norm_1(x)
        x = self.relu(x)

        x = self.conv_2(x)
        x = self.batch_norm_2(x)
        x = self.relu(x)

        x = self.max_pool_1(x)

        x = self.conv_3(x)
        x = self.batch_norm_3(x)
        x = self.relu(x)

        x = self.conv_4(x)
        x = self.batch_norm_4(x)
        x = self.relu(x)

        x = self.max_pool_2(x)

        x = self.conv_5(x)
        x = self.batch_norm_5(x)
        x = self.relu(x)

        x = self.conv_6(x)
        x = self.batch_norm_6(x)
        x = self.relu(x)

        x = self.conv_7(x)
        x = self.batch_norm_7(x)
        x = self.relu(x)

        x = self.max_pool_3(x)

        x = self.conv_8(x)
        x = self.batch_norm_8(x)
        x = self.relu(x)

        x = self.conv_9(x)
        x = self.batch_norm_9(x)
        x = self.relu(x)

        x = self.conv_10(x)
        x = self.batch_norm_10(x)
        x = self.relu(x)

        x = self.max_pool_4(x)

        x = self.conv_11(x)
        x = self.batch_norm_11(x)
        x = self.relu(x)

        x = self.conv_12(x)
        x = self.batch_norm_12(x)
        x = self.relu(x)

        x = self.conv_13(x)
        x = self.batch_norm_13(x)
        x = self.relu(x)

        x = self.max_pool_5(x)

        # reshape for fully connected layer
        x = x.view(x.shape[0], 4 * 4 * 512)

        x = self.linear_14(x)
        #x = self.drop_out_14(x)
        #x = self.relu(x)

        x = self.linear_15(x)
        #x = self.drop_out_15(x)
        #x = self.relu(x)

        # x = self.linear_16(x)
        # x = self.relu(x)
        x = self.softmax(x)

        return x

    def _initialize_weights(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
                if m.bias is not None:
                    nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.BatchNorm2d) and m.affine:
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear):
                nn.init.normal_(m.weight, 0, 0.01)
                nn.init.constant_(m.bias, 0)

def load_model():
    # This is the function to be filled. Your returned model needs to be an instance of subclass of torch.nn.Module
    # Model needs to be accepting tensors of shape [B, 3, 128, 128], where B is batch_size, which are in a range of [0-1] and type float32
    # It should be possible to pass in cuda tensors (in that case, model.cuda() will be called first).
    # The model will return scores (or probabilities) for each of the 10 classes, i.e a tensor of shape [B, 10]
    # The resulting tensor should have same device and dtype as incoming tensor

    directory = os.path.abspath(os.path.dirname(__file__))

    # The model should be trained in advance and in this function, you should instantiate model and load the weights into it:
    model = Model(3)
    model.load_state_dict(torch.load(directory + '/weights.pts', map_location='cpu'))


    # For more info on storing and loading weights, see https://pytorch.org/tutorials/beginner/saving_loading_models.html
    return model


def main(lr=0.0001, momentum=0., batch_size=100, epochs=150, find_hyperparams=False, save_weights=False):
    model_path = os.path.abspath("/home.nfs/novotp23/VIR/weights.pts")

    best_accuracy = -10000
    best_loss = 10000

    trn_data = data.ClassificationDataset(pkl_name="trn.pkl", augmentation=True)
    val_data = data.ClassificationDataset(pkl_name="val.pkl", augmentation=False)

    if find_hyperparams:
        trn_loader = torch.utils.data.DataLoader(trn_data, batch_size=batch_size, shuffle=False)
    else:
        trn_loader = torch.utils.data.DataLoader(trn_data, batch_size=batch_size, shuffle=True)

    val_loader = torch.utils.data.DataLoader(val_data, batch_size=100, shuffle=True)

    loss_function = nn.CrossEntropyLoss()

    model = Model(3)

    if torch.cuda.is_available():
        device = torch.device('cuda', 1)
    else:
        device = torch.device('cpu')

    model = model.to(device)
    optimizer = torch.optim.SGD(model.parameters(), lr=lr)

    accuracy_list = []

    for epoch in range(epochs):
        ''' Store predictions and labels for Accuracy calculation'''
        pred_list = []
        label_list = []
        pred_val_list = []
        label_val_list = []

        ''' Iterate over trn_dataloader to sample data and labels '''
        model.train()
        for batch in trn_loader:
            x = batch['data'].to(device)
            y = batch['label'].to(device)

            output = model.forward(x)

            loss = loss_function(output, y)

            loss.backward()

            optimizer.step()
            optimizer.zero_grad()

            pred_list.append(torch.argmax(output, dim=1))
            label_list.append(y)

            if find_hyperparams:
                break

        # compute accuracy on validation data
        model.eval()
        loss_eval = 0
        for batch in val_loader:
            with torch.no_grad():
                x = batch['data'].to(device)
                y = batch['label'].to(device)

                output = model.forward(x)
                loss_eval += loss_function(output, y)

                pred_val_list.append(torch.argmax(output, dim=1))
                label_val_list.append(y)
        # compute validation data accuracy
        pred_val_list = torch.cat(pred_val_list)
        label_val_list = torch.cat(label_val_list)

        accuracy_val = (pred_val_list == label_val_list).float().mean()

        # compute training data accuracy
        pred_list = torch.cat(pred_list)
        label_list = torch.cat(label_list)

        accuracy = (pred_list == label_list).float().mean()

        if find_hyperparams:
            print(f"Epoch {epoch} \t Test accuracy: {accuracy * 100:.3f} % ")
        else:
            print(f"Epoch {epoch} \t Test accuracy: {accuracy * 100:.3f} % \n Validation accuracy : {accuracy_val * 100:.3f} %, loss eval: {loss_eval}")

        if loss_eval < best_loss and save_weights:
            best_loss = loss_eval
            print(f"model saved in {model_path}")
            torch.save(model.state_dict(), model_path)

        accuracy_list.append(accuracy * 100)

        if accuracy_val > best_accuracy:
            best_accuracy = accuracy_val
            # save model

    return best_loss

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    lr = [0.03]
    for i in range(len(lr)):
        print(f"lr: {lr[i]}")
        main(lr=lr[i], batch_size=100, epochs=200, find_hyperparams=False, save_weights=True)
